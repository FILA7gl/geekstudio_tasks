import django_filters
from rest_framework import filters

from . import models


class CustomProductFilter(django_filters.FilterSet):
    """ Фильтрация по категории и по диапазону цен """

    price_min = django_filters.NumberFilter(field_name='price', lookup_expr='gte')
    price_max = django_filters.NumberFilter(field_name='price', lookup_expr='lte')
    category_title = django_filters.CharFilter(field_name='category__title', lookup_expr='icontains')

    class Meta:
        model = models.Product
        fields = ['price_min', 'price_max']


class CustomProductSearch(filters.SearchFilter):
    """ Поиск продуктов по названию """

    def get_search_fields(self, view, request):
        return ['title']
