from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from . import services, serializers, permissions
from .filters import CustomProductSearch, CustomProductFilter


class ProductViewSet(ModelViewSet):
    queryset = (services.ProductService.objects()
                .select_related('promotion', 'owner')
                .prefetch_related('categories')
                .all())
    serializer_class = serializers.ProductSerializer
    permission_classes = [permissions.IsAdminOrTenantUser]
    filter_backends = [DjangoFilterBackend, CustomProductSearch, filters.SearchFilter]
    filterset_class = CustomProductFilter

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)  # Установка текущего пользователя как владельца


class CategoryViewSet(ModelViewSet):
    queryset = services.CategoryService().filter(is_approved=True)
    serializer_class = serializers.CategorySerializer
    permission_classes = [permissions.IsAdminAndTenant]


class PromotionViewSet(ModelViewSet):
    queryset = services.PromotionService.get_all()
    serializer_class = serializers.PromotionSerializer
    permission_classes = [permissions.IsAdminAndTenant]


class GlobalSearchAPIView(APIView):

    def get(self, request):
        searched_data = services.SearchService.search_title(request)

        return Response(searched_data)
