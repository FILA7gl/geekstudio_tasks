# Generated by Django 4.2.4 on 2023-09-05 05:08

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100, verbose_name='Название')),
                ('is_approved', models.BooleanField(default=False, verbose_name='Одобрено')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='Promotion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=155, verbose_name='Название')),
                ('description', models.TextField(verbose_name='Описание')),
                ('is_discount', models.BooleanField(default=False, verbose_name='Скидка')),
                ('discount', models.PositiveSmallIntegerField(default=0, validators=[django.core.validators.MaxValueValidator(100)], verbose_name='% скидки')),
            ],
            options={
                'verbose_name': 'Акция',
                'verbose_name_plural': 'Акции',
                'ordering': ('-id',),
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='product_images')),
                ('title', models.CharField(max_length=150, verbose_name='Название')),
                ('description', models.TextField(verbose_name='Описание')),
                ('price', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='Цена')),
                ('is_available', models.BooleanField(default=True, verbose_name='В наличии')),
                ('color', models.CharField(choices=[('red', 'Красный'), ('blue', 'Синий'), ('green', 'Зеленый'), ('yellow', 'Желтый'), ('orange', 'Оранжевый'), ('purple', 'Фиолетовый'), ('pink', 'Розовый'), ('brown', 'Коричневый'), ('white', 'Белый'), ('black', 'Черный'), ('gray', 'Серый')], max_length=50, verbose_name='Цвет')),
                ('weight', models.PositiveIntegerField(verbose_name='Вес')),
                ('categories', models.ManyToManyField(blank=True, to='products.category', verbose_name='Категории')),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Владелец')),
                ('promotion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='products.promotion', verbose_name='Акция')),
            ],
            options={
                'verbose_name': 'Продукт',
                'verbose_name_plural': 'Продукты',
                'ordering': ('-id',),
            },
        ),
    ]
