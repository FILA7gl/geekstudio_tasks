from PIL import Image
from django.core.cache import cache
from django.db.models import Q

from . import serializers
from ..common.services import Service

""" Получение всех обьектов """


class ProductService(Service):
    from .models import Product
    model = Product


class CategoryService(Service):
    from .models import Category
    model = Category


class PromotionService(Service):
    from .models import Promotion
    model = Promotion


class ImageService:

    @staticmethod
    def compress_image(image):
        """ Функция сжатия картинки """

        max_width = 700
        img = Image.open(image.path)
        if img.width > max_width:
            aspect_ratio = img.width / img.height
            new_width = max_width
            new_height = int(new_width / aspect_ratio)
            img = img.resize((new_width, new_height), Image.LANCZOS)

        img.save(image.path, quality=90)


class ProductModelService:
    @staticmethod
    def get_discount_price(product):
        try:
            if product.promotion.is_discount:
                discount_price = round(float(product.price) - (float(product.price) /
                                                               100 * product.promotion.discount), 2)
                return discount_price
        except:
            return None


class PromotionSerializerService:
    @staticmethod
    def get_promotion_details(product):
        try:
            promotion = product.promotion
            return {
                'title': promotion.title,
                'description': promotion.description,
                'is_discount': promotion.is_discount,
                'discount': promotion.discount
            }
        except:
            return None


class SearchService:
    @staticmethod
    def search_title(request):
        """ Поиск по всем моделям по названию, и кеширование данных """

        title = request.query_params.get('title', '')

        cached_data = cache.get(title)

        if not cached_data:
            product_results = ProductService.filter(Q(title__istartswith=title))
            category_results = CategoryService.filter(Q(title__istartswith=title))
            promotion_results = PromotionService.filter(Q(title__istartswith=title))

            serialized_data = {
                'products': serializers.ProductSerializer(product_results, many=True).data,
                'categories': serializers.CategorySerializer(category_results, many=True).data,
                'promotions': serializers.PromotionSerializer(promotion_results, many=True).data
            }
            cache.set(title, serialized_data)

            return serialized_data

        return cached_data
