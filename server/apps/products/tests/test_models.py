# from django.test import TestCase
# from server.apps.products.models import Product, Category
#
#
# class ProductModelTestCase(TestCase):
#     def setUp(self):
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#         self.product_1 = Product.objects.create(
#             image=None,
#             title='product_one',
#             description='bla bla bla',
#             price=100,
#             is_available=True,
#             color='red',
#             weight=8,
#             discount=True,
#
#         )
#         self.product_1.categories.add(self.category_1)
#
#         self.product_2 = Product.objects.create(
#             image=None,
#             title='product_two',
#             description='bla bla bla blaaaa',
#             price=150,
#             is_available=False,
#             color='green',
#             weight=12,
#             discount=False,
#         )
#         self.product_2.categories.add(self.category_2)
#
#     def test_creation_product(self):
#         self.assertEqual(self.product_1.title, 'product_one')
#         self.assertEqual(self.product_2.is_available, False)
#         self.assertTrue(self.product_1.categories.filter(title=self.category_1.title).exists())
#         self.assertEqual(Product.objects.all().count(), 2)
#
#
# class ProductCategoryModelTestCase(TestCase):
#     def setUp(self):
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#     def test_creation_product_category(self):
#         self.assertEqual(self.category_1.title, 'category_one')
#         self.assertTrue(Category.objects.all().count(), 2)
