# from django.contrib.auth.models import User
# from django.core.paginator import Paginator
# from django.test import TestCase, Client
# from django.urls import reverse
# from rest_framework import status
#
# from server.apps.products.models import Category, Product
# from server.apps.products.serializers import ProductSerializer, CategorySerializer
# from server.apps.users.models import CustomUser
#
#
# class ProductAPITestCase(TestCase):
#     def setUp(self):
#         self.client = Client()
#
#         self.admin = CustomUser.objects.create(
#             username='root',
#             password='gg12345678',
#             role='admin'
#         )
#         self.tenant = CustomUser.objects.create(
#             username='tenant',
#             password='12345678',
#             role='tenant'
#         )
#         self.buyer = CustomUser.objects.create(
#             username='buyer',
#             password='12345678',
#         )
#
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#         self.product_1 = Product.objects.create(
#             image=None,
#             title='product_one',
#             description='bla bla bla',
#             price=100,
#             is_available=True,
#             color='red',
#             weight=8,
#             discount=True,
#
#         )
#         self.product_1.categories.add(self.category_1)
#
#         self.product_2 = Product.objects.create(
#             image=None,
#             title='product_two',
#             description='bla bla bla blaaaa',
#             price=150,
#             is_available=False,
#             color='green',
#             weight=12,
#             discount=False,
#         )
#         self.product_2.categories.add(self.category_2)
#
#     def test_list_products(self):
#         url = reverse('product-list')
#         response = self.client.get(url, {'request': '10'})
#
#         serializer_data = ProductSerializer([self.product_2, self.product_1], many=True).data
#
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertIn('count', response.data)
#         self.assertIn('next', response.data)
#         self.assertIn('results', response.data)
#
#         products = response.data['results']
#         self.assertEqual(products, serializer_data)
#
#         paginator = Paginator(Product.objects.all(), 10)
#
#         self.assertEqual(paginator.num_pages, 1)
#
#     def test_create_product(self):
#         url = reverse('product-list')
#         request = {
#             "title": "product_one",
#             "description": "bla bla bla",
#             "price": '100.00',
#             "is_available": True,
#             "color": "red",
#             "weight": 8,
#             "discount": 10,
#         }
#
#         response = self.client.post(url, data=request)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         user = CustomUser.objects.get(username='tenant')
#         self.client.force_login(user)
#
#         auth_user_response = self.client.post(url, data=request)
#         self.assertEqual(auth_user_response.status_code, status.HTTP_201_CREATED)
#
#         expected_data = {
#             'id': 3,
#             "image": None,
#             "title": "product_one",
#             "description": "bla bla bla",
#             "price": '100.00',
#             "is_available": True,
#             "color": "red",
#             "weight": 8,
#             "discount": 10,
#             "categories": [],
#             "discount_price": None,
#             "owner": 'id: 2, username: tenant, role: tenant'
#         }
#         self.assertEqual(expected_data, auth_user_response.data)
#
#     # def test_update_product(self):
#     #     url = reverse('product-detail', args=[self.product_1.id])
#     #
#     #     user = CustomUser.objects.get(username='root')
#     #     self.client.force_login(user)
#     #
#     #     request = {
#     #         "title": "product_new",
#     #         "description": "bla bla bla",
#     #         "price": '100.00',
#     #         "is_available": True,
#     #         "color": "red",
#     #         "weight": 8,
#     #         "discount": 10,
#     #
#     #     }
#     #     auth_user_response = self.client.put(url, json=request)
#     #     self.product_1.refresh_from_db()
#     #
#     #     self.assertEqual(auth_user_response.status_code, status.HTTP_200_OK)
#     #
#     #     updated_product = Product.objects.get(id=self.product_1.id)
#     #
#     #     self.assertEqual('product_new', updated_product.title)
#
#     def test_detail_product(self):
#         url = reverse('product-detail', args=[self.product_1.id])
#         response = self.client.get(url)
#         self.assertEqual(response.data['title'], 'product_one')
#
#     def test_delete_product(self):
#         url = reverse('product-detail', args=[self.product_1.id])
#
#         user = CustomUser.objects.get(username='root')
#         self.client.force_login(user)
#
#         response = self.client.delete(url)
#
#         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
#
#
# class ProductCategoryAPITestCase(TestCase):
#     def setUp(self):
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#         self.user = CustomUser.objects.create(
#             username='root',
#             password='gg12345678',
#             is_staff=True,
#             role='tenant'
#         )
#
#     # def test_list_product_category(self):
#     #     url = reverse('category-list')
#     #     response = self.client.get(url)
#     #
#     #     self.assertEqual(response.status_code, status.HTTP_200_OK)
#     #
#     #     serializer_data = CategorySerializer([self.category_1, self.category_2], many=True).data
#     #     categories = response.data['results']
#     #
#     #     self.assertEqual(categories, serializer_data)
#
#     def test_create_product_category(self):
#         url = reverse('category-list')
#         request = {'title': 'cat one'}
#
#         response = self.client.post(url, data=request)
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
#
#         user = CustomUser.objects.get(username='root')
#         self.client.force_login(user)
#
#         auth_user_response = self.client.post(url, data=request)
#         self.assertEqual(auth_user_response.status_code, status.HTTP_201_CREATED)
#
#         self.assertEqual(auth_user_response.data['title'], 'cat one')
#
#     # def test_detail_product_category(self):
#     #     url = reverse('category-detail', args=[self.category_1.id])
#     #
#     #     response = self.client.get(url)
#     #
#     #     self.assertEqual(response.status_code, status.HTTP_200_OK)
#     #     self.assertEqual(response.data['title'], 'category_one')
#
#     # def test_delete_product_category(self):
#     #     url = reverse('category-detail', args=[self.category_2.id])
#     #
#     #     user = CustomUser.objects.get(username='root')
#     #     self.client.force_login(user)
#     #
#     #     response = self.client.delete(url)
#     #
#     #     self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
#
#     # def test_update_product_category(self):
#     #     url = reverse('category-detail', args=[self.category_1.id])
#     #
#     #     user = User.objects.get(username='root')
#     #     self.client.force_login(user)
#     #
#     #     request = {
#     #         'title': 'new_cat'
#     #     }
#     #     response = self.client.put(url, json=request)
#     #
#     #     self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#
# class ProductFilterAPITestCase(TestCase):
#     def setUp(self):
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#         self.product_1 = Product.objects.create(
#             image=None,
#             title='product_one',
#             description='bla bla bla',
#             price=100,
#             is_available=True,
#             color='red',
#             weight=8,
#             discount=True,
#
#         )
#         self.product_1.category.add(self.category_1)
#
#         self.product_2 = Product.objects.create(
#             image=None,
#             title='product_two',
#             description='bla bla bla blaaaa',
#             price=150,
#             is_available=False,
#             color='green',
#             weight=12,
#             discount=False,
#         )
#         self.product_2.category.add(self.category_2)
#
#     # def test_product_filter(self):
#     #     url = reverse('product-list')
#     #     response = self.client.get(url, {'category__title': 'category_one'})
#     #     print(response.data)
#     #     self.assertEqual(len(response.data), 1)
#
#     # def test_search_product(self):
#     #     url = reverse('product-list')
#     #     response = self.client.get(url, {'title': 'product_one'})
#     #     print(Product.objects.all())
#     #     print(response.data['results'])
#     #
#     #     self.assertEqual(status.HTTP_200_OK, response.status_code)
#     #     self.assertEqual(1, len(response.data['results']))
