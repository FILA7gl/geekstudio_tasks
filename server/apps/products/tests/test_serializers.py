# from django.test import TestCase
# from server.apps.products.models import Category, Product
# from server.apps.products.serializers import ProductSerializer, CategorySerializer
#
#
# class ProductSerializerTestCase(TestCase):
#     def setUp(self):
#
#         self.context = ('request', {'request': 100})
#
#         self.category_1 = Category.objects.create(title='category_one')
#
#         self.product_1 = Product.objects.create(
#             image=None,
#             title='product_one',
#             description='bla bla bla',
#             price=100,
#             is_available=True,
#             color='red',
#             weight=8,
#             discount=10,
#
#
#         )
#         self.product_1.categories.add(self.category_1)
#
#         self.product_2 = Product.objects.create(
#             image=None,
#             title='product_two',
#             description='bla bla bla blaaaa',
#             price=150,
#             is_available=False,
#             color='green',
#             weight=12,
#             discount=False,
#         )
#
#     def test_product_ok(self):
#         serializer_data = ProductSerializer([self.product_1, self.product_2], many=True, context=self.context).data
#         expected_data = [
#             {
#                 "id": self.product_1.id,
#                 "image": None,
#                 "title": "product_one",
#                 "description": "bla bla bla",
#                 "price": '100.00',
#                 "is_available": True,
#                 "color": "red",
#                 "weight": 8,
#                 "discount": 10,
#                 "categories": [self.category_1.title],
#                 "discount_price": None,
#                 "owner": None},
#             {
#                 "id": self.product_2.id,
#                 "image": None,
#                 "title": "product_two",
#                 "description": "bla bla bla blaaaa",
#                 "price": '150.00',
#                 "is_available": False,
#                 "color": "green",
#                 "weight": 12,
#                 "discount": 0,
#                 "categories": [],
#                 "discount_price": None,
#                 "owner": None
#             }
#         ]
#         self.assertEqual(expected_data, serializer_data)
#         self.assertTrue(2, len(serializer_data))
#
#
# class ProductCategorySerializerTestCase(TestCase):
#     def setUp(self):
#         self.category_1 = Category.objects.create(title='category_one')
#         self.category_2 = Category.objects.create(title='category_two')
#
#     def test_category_ok(self):
#         serializer_data = CategorySerializer([self.category_1, self.category_2], many=True).data
#         expected_data = [
#             {
#                 'id': self.category_1.id,
#                 'title': 'category_one'
#             },
#             {
#                 'id': self.category_2.id,
#                 'title': 'category_two'
#             }
#         ]
#
#         self.assertTrue(len(serializer_data), 2)
#         self.assertEqual(expected_data, serializer_data)
