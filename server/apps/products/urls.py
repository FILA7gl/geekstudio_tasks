from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import ProductViewSet, CategoryViewSet, PromotionViewSet, GlobalSearchAPIView

router = SimpleRouter()
router.register('products', ProductViewSet, basename='product')
router.register('categories', CategoryViewSet, basename='category')
router.register('promotions', PromotionViewSet, basename='promotion')

urlpatterns = [
    path('', include(router.urls)),
    path('search/', GlobalSearchAPIView.as_view())
]
