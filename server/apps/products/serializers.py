from rest_framework import serializers
from . import models


class PromotionSerializer(serializers.ModelSerializer):
    discount = serializers.IntegerField(min_value=0, max_value=100)

    class Meta:
        model = models.Promotion
        fields = ('id', 'title', 'description', 'is_discount', 'discount')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ('id', 'title')


class ProductSerializer(serializers.ModelSerializer):
    weight = serializers.IntegerField(min_value=0)
    price = serializers.DecimalField(min_value=0, max_digits=7, decimal_places=2)
    owner = serializers.CharField(read_only=True)
    categories = serializers.SlugRelatedField(
        many=True,
        slug_field='title',
        allow_null=True,
        queryset=models.Category.objects.filter(is_approved=True))

    promotion = serializers.SlugRelatedField(
        slug_field='title',
        required=False,
        allow_null=True,
        queryset=models.Promotion.objects.all())
    promotion_details = serializers.SerializerMethodField()

    class Meta:
        model = models.Product
        fields = ('id', 'image', 'title', 'description', 'price',
                  'is_available', 'color', 'weight', 'categories',
                  'promotion', 'promotion_details',
                  'discount_price', 'owner')

    @staticmethod
    def get_promotion_details(product):
        if product.promotion:
            from .services import PromotionSerializerService
            return PromotionSerializerService.get_promotion_details(product)
