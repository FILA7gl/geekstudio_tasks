from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAdminOrTenantUser(BasePermission):
    """ Проверка на админа и арендатора """

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return bool(request.user.is_authenticated and (request.user.role == 'tenant' or
                                                       (request.method != 'POST' and request.user.role == 'admin'))
                    )

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return bool(request.user.is_authenticated and (request.user.role == 'admin' or obj.owner))


class IsAdminAndTenant(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return bool(request.user.is_authenticated and request.user.role == 'tenant')
