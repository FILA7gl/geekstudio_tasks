from django.db import models
from django.core.validators import MaxValueValidator
from .celery_tasks import send_promotional_emails
from .choices import COLOR_CHOICES
from ..users.models import CustomUser


class Promotion(models.Model):
    title = models.CharField(max_length=155, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    is_discount = models.BooleanField(default=False, verbose_name='Скидка')
    discount = models.PositiveSmallIntegerField(default=0, verbose_name='% скидки',
                                                validators=[MaxValueValidator(100)])

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'


class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название')
    is_approved = models.BooleanField(default=False, verbose_name='Одобрено')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Product(models.Model):
    image = models.ImageField(upload_to='product_images', blank=True, null=True)
    title = models.CharField(max_length=150, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    price = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Цена')
    is_available = models.BooleanField(default=True, verbose_name='В наличии')
    color = models.CharField(choices=COLOR_CHOICES, verbose_name='Цвет', max_length=50)
    weight = models.PositiveIntegerField(verbose_name='Вес')
    promotion = models.ForeignKey(Promotion, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Акция')
    categories = models.ManyToManyField(Category, verbose_name='Категории', blank=True)
    owner = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Владелец')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def discount_price(self):
        from .services import ProductModelService
        return ProductModelService.get_discount_price(self)

    def save(self, *args, **kwargs):
        if self.promotion:
            old_product = None
            if self.pk:
                old_product = Product.objects.get(pk=self.pk)

            if self.promotion != old_product.promotion and self.promotion:
                send_promotional_emails.apply_async(countdown=10)
        super().save(*args, **kwargs)

        if self.image:
            from .services import ImageService

            ImageService.compress_image(self.image)
