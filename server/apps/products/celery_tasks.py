from celery import shared_task
from django.core.mail import send_mail

from server.apps.users.models import CustomUser
from server.settings.env_reader import env


@shared_task
def send_promotional_emails():
    from server.apps.products.models import Product

    users = CustomUser.objects.filter(role='buyer')
    promotion_products = Product.objects.filter(promotion__title__isnull=False)
    email_host = env("EMAIL_HOST_USER")

    try:
        for user in users:
            subject = f"Добрый день {user.username}!"
            to_email = user.email
            message = "Список акционных продуктов:\n\n"

            for product in promotion_products:
                message += f'{product.title}:\n\t- {product.promotion.description}\n\n'

            send_mail(
                subject=subject,
                message=message,
                from_email=email_host,
                recipient_list=[to_email],
            )
            print(f'Письмо отправлено пользователю: {user.username}')
    except Exception as e:
        print(f'Не получилось отправить письмо ошибка: {str(e)}')
