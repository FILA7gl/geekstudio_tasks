from django.contrib import admin

from . import models


@admin.register(models.Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'is_available', 'promotion', 'price', 'owner')
    list_filter = ('price', 'is_available', 'promotion')
    search_fields = ('title',)
    list_per_page = 10

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('owner', 'promotion',)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'is_approved')
    list_filter = ('is_approved',)
    search_fields = ('title',)
    readonly_fields = ('title',)
    list_per_page = 10

    def has_add_permission(self, request):
        return False


@admin.register(models.Promotion)
class PromotionAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'is_discount', 'discount')
    list_filter = ('is_discount',)
    search_fields = ('title',)
    list_per_page = 10

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
