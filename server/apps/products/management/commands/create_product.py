import csv
from django.core.management.base import BaseCommand
from server.apps.products import models


class Command(BaseCommand):
    help = 'Create products from a CSV file'

    def add_arguments(self, parser):
        parser.add_argument('csv_file', type=str, help='Path to the CSV file')

    def handle(self, *args, **options):
        csv_file_path = options['csv_file']

        with open(csv_file_path, 'r') as file:
            reader = csv.DictReader(file)

            for row in reader:
                product = models.Product.objects.create(
                    title=row['title'],
                    description=row['Description'],
                    price=row['Price'],
                    is_available=row['is available'],
                    color=row['Color'],
                    weight=row['Weight'],
                )

                if row['Categories']:
                    categories = row['Categories'].split(',')
                    for category_name in categories:
                        category, created = models.Category.objects.get_or_create(title=category_name.strip())
                        product.categories.add(category)

                if row['Promotion']:
                    try:
                        promotion = models.Promotion.objects.get(title=row['Promotion'])
                        product.promotion = promotion
                    except models.Promotion.DoesNotExist:
                        self.stdout.write(f'ID: {row["ID"]}, Акция не найдена!')
                        continue

                if row['Owner']:
                    try:
                        owner = models.CustomUser.objects.get(username=row['Owner'])
                        product.owner = owner
                    except models.CustomUser.DoesNotExist:
                        self.stdout.write(f'ID: {row["ID"]}, Владелец не найден!')
                        continue

                product.save()

        self.stdout.write(self.style.SUCCESS('Successfully created products from the CSV file.'))
