from django.db.models import QuerySet


class Service:
    model = None

    @classmethod
    def get_or_none(cls, *args, **kwargs) -> QuerySet[model]:
        try:
            cls.model.objects.get(*args, **kwargs)
        except:
            cls.model.objects.none()

    @classmethod
    def get_all(cls, *args, **kwargs):
        return cls.model.objects.all()

    @classmethod
    def filter(cls, *args, **kwargs):
        return cls.model.objects.filter(*args, **kwargs)

    @classmethod
    def objects(cls):
        return cls.model.objects
