from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet


class NoCreateModelViewSet(mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           mixins.ListModelMixin,
                           GenericViewSet):
    """ Кастомный класс без метода создания """

    pass
