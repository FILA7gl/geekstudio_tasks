from rest_framework import serializers
from . import services


class GoogleSocialAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField()

    def validate_auth_token(self, auth_token):
        return services.GoogleAuthSerializerService.validate_auth_token(auth_token)
