import random

from django.contrib.auth import authenticate
from google.auth.transport import requests
from google.oauth2 import id_token

from rest_framework import serializers
from server.apps.users import models
from server.settings.env_reader import env


class GoogleAuthService:
    model = models.CustomUser

    @staticmethod
    def validate(auth_token):
        try:
            id_info = id_token.verify_oauth2_token(auth_token, requests.Request())

            if 'accounts.google.com' in id_info['iss']:
                return id_info

        except:
            return 'The token is invalid'

    @staticmethod
    def generate_username(username):
        usernames = models.CustomUser.objects.filter(username=username)
        if not usernames.exists():
            return username
        return f'{username}{random.randint(1000, 10000)}'

    @staticmethod
    def register_social_user(provider, email, username, first_name, last_name):
        registered_user = models.CustomUser.objects.filter(email=email)

        if registered_user.exists():
            user = models.CustomUser.objects.get(email=email)
            return {'username': user.username,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'email': user.email,
                    'tokens': user.tokens()}

        else:
            new_username = GoogleAuthService.generate_username(username)
            user = models.CustomUser.objects.create_user(username=new_username, email=email, first_name=first_name,
                                                         last_name=last_name, password=env('SOCIAL_SECRET'),
                                                         auth_provider=provider)
            user.save()
            new_user = authenticate(username=username, email=email, password=env('SOCIAL_SECRET'))

            return {'message': 'registration success',
                    'username': user.username,
                    'first_name': new_user.first_name,
                    'last_name': new_user.last_name,
                    'email': new_user.email,
                    'tokens': new_user.tokens()}


class GoogleAuthSerializerService:
    @staticmethod
    def validate_auth_token(auth_token):
        user_data = GoogleAuthService.validate(auth_token)
        print(user_data)
        try:
            user_data['sub']
        except:
            raise serializers.ValidationError('The token is invalid')

        email = user_data.get('email')
        username = user_data.get('name')
        first_name = user_data.get('given_name')
        last_name = user_data.get('family_name')
        return GoogleAuthService.register_social_user(provider='google', email=email, username=username,
                                                      first_name=first_name, last_name=f'{last_name}')
