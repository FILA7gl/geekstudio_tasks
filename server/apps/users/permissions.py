from rest_framework.permissions import BasePermission


class IsAdminOrReadOnly(BasePermission):

    """ Проверка на админа иначе для чтения """

    def has_permission(self, request, view):

        return bool(request.user.is_authenticated and request.user.role == 'admin')

    def has_object_permission(self, request, view, obj):

        return bool(request.user.is_authenticated and request.user.role == 'admin')
