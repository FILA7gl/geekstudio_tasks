from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView

from .serializers import UserRegistrationSerializer, UserLoginSerializer, UserProfileSerializer
from .permissions import IsAdminOrReadOnly
from ..common.views import NoCreateModelViewSet
from .services import UserService


class UserRegistrationAPIView(CreateAPIView):
    queryset = UserService.objects().only('username', 'first_name',
                                          'last_name', 'email', 'password')
    serializer_class = UserRegistrationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = UserService.create_user(**serializer.data)
        if user:
            return Response(
                data={'message': 'success'},
                status=status.HTTP_201_CREATED
            )
        return Response(data={'error': 'not success'}, status=status.HTTP_400_BAD_REQUEST)


class UserLoginAPIView(CreateAPIView):
    queryset = UserService.get_all().only('email', 'password')
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.validated_data)

        if user:
            return Response(data={'message': 'success',
                                  'tokens': user.tokens()},
                            status=status.HTTP_200_OK)

        return Response(data={'message': 'Неправильное имя пользователя или пароль!'},
                        status=status.HTTP_404_NOT_FOUND)


class UserProfileViewSet(NoCreateModelViewSet):
    queryset = UserService.get_all().only('id', 'username', 'first_name', 'last_name', 'email', 'role')
    serializer_class = UserProfileSerializer
    permission_classes = [IsAdminOrReadOnly]
