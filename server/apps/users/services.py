from rest_framework import serializers

from server.apps.users import models
from ..common.services import Service


class UserService(Service):
    model = models.CustomUser

    @classmethod
    def create_user(cls, username, first_name,
                    last_name, password):
        return cls.model.objects.create_user(username=username, first_name=first_name,
                                             last_name=last_name, password=password)


class UserSerializerService:
    @staticmethod
    def validate_username(username):  # проверка на уникальность имени пользователя
        if models.CustomUser.objects.filter(username=username).exists():
            raise serializers.ValidationError('Имя пользователя занято!')
        return username

    @staticmethod
    def validate_email(email):
        if models.CustomUser.objects.filter(email=email).exists():
            raise serializers.ValidationError('Почта занята!')
        return email

    @staticmethod
    def validate(data):
        if data['password'] != data['password_confirm']:
            raise serializers.ValidationError('Пароли не совпадают!')
        if len(data['password']) < 8:
            raise serializers.ValidationError('минимум 8 символов!')
        return data
