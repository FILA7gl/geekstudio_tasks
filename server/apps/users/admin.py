from django.contrib import admin
from .models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', 'role', 'auth_provider')
    search_fields = ('username', 'email')
    list_filter = ('role', 'auth_provider', 'is_staff')
    readonly_fields = ('username', 'first_name', 'last_name', 'password', 'email',
                       'auth_provider', 'last_login')
    list_per_page = 10

    def has_add_permission(self, request):
        return False
