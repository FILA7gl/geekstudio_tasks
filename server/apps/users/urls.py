from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import UserProfileViewSet, UserLoginAPIView, UserRegistrationAPIView

router = SimpleRouter()
router.register('', UserProfileViewSet, basename='user')

urlpatterns = [
    path('registration/', UserRegistrationAPIView.as_view(), name='registration'),
    path('login/', UserLoginAPIView.as_view()),

    path('', include(router.urls))
]
