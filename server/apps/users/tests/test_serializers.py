from django.test import TestCase

from server.apps.users.models import CustomUser
from ..serializers import UserProfileSerializer


class UserSerializerTestCase(TestCase):
    def setUp(self):
        self.user_1 = CustomUser.objects.create_user(username='user_one', password='12345678',
                                                     first_name='first_user', last_name='first_user', role='admin')
        self.user_2 = CustomUser.objects.create_user(username='user_two', password='12345678',
                                                     first_name='second_user', last_name='second_user')

    def test_ok(self):
        serializer_data = UserProfileSerializer([self.user_1, self.user_2], many=True).data

        expected_data = [{
            'id': self.user_1.id,
            'username': 'user_one',
            'first_name': 'first_user',
            'last_name': 'first_user',
            'role': 'admin'
        },
            {
                'id': self.user_2.id,
                'username': 'user_two',
                'first_name': 'second_user',
                'last_name': 'second_user',
                'role': 'buyer'
            }]

        self.assertEqual(2, len(serializer_data))
        self.assertEqual(expected_data, serializer_data)
