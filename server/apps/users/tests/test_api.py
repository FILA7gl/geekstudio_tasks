from django.test import TestCase
from rest_framework import status
from ..serializers import UserProfileSerializer
from server.apps.users.models import CustomUser


class UserTestCase(TestCase):

    def setUp(self):
        self.user_1 = CustomUser.objects.create_user(username='user_one', first_name='first_name',
                                                     last_name='last_name', password='12345678', role='admin')

    def test_registration(self):
        url = '/api/v1/users/registration/'

        request = {
            'username': 'user_test',
            'first_name': 'first_user',
            'last_name': 'first_user',
            'password': '12345678',
            'password_confirm': '12345678'
        }

        response = self.client.post(url, data=request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CustomUser.objects.filter(username='user_one').count(), 1)

    # def test_user_login(self):
    #     url = '/api/v1/users/login/'
    #
    #     request = {
    #         'username': 'user_one',
    #         'password': '12345678'
    #     }
    #
    #     response = self.client.post(url, data=request)
    #
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_profile(self):
        url = '/api/v1/users/'

        user = CustomUser.objects.get(username='user_one')
        self.client.force_login(user=user)
        response = self.client.get(url)

        serializer_data = UserProfileSerializer(self.user_1).data
        expected_data = {
            'id': self.user_1.id,
            'username': 'user_one',
            'first_name': 'first_name',
            'last_name': 'last_name',
            'role': 'admin'
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer_data, expected_data)
