from django.test import TestCase

from server.apps.users.models import CustomUser


class UserModelTestCase(TestCase):
    def setUp(self):
        self.user_1 = CustomUser.objects.create_user(username='user_one', password='12345678',
                                                     first_name='first_user', last_name='first_user')
        self.user_2 = CustomUser.objects.create_user(username='user_two', password='12345678',
                                                     first_name='second_user', last_name='second_user')

    def test_creation_user(self):
        self.assertEqual(2, CustomUser.objects.all().count())
        self.assertEqual('user_one', CustomUser.objects.get(username='user_one').username)
