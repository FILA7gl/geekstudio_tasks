from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from rest_framework_simplejwt.tokens import RefreshToken

from .managers import CustomUserManager
from .choices import ROLE_CHOICES

AUTH_PROVIDERS = {'google': 'google', 'email': 'email'}


class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=155, validators=[UnicodeUsernameValidator],
                                unique=True, verbose_name='Имя пользователя')
    first_name = models.CharField(max_length=155, verbose_name='Имя')
    last_name = models.CharField(max_length=155, verbose_name='Фамилия')
    email = models.EmailField(max_length=255, unique=True, verbose_name='Почта', null=True)
    role = models.CharField(choices=ROLE_CHOICES, default='buyer', max_length=20, verbose_name='Роль')
    is_staff = models.BooleanField(default=False, verbose_name='Сотрудник')
    auth_provider = models.CharField(max_length=155, default=AUTH_PROVIDERS.get('email'))
    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def __str__(self):
        return self.username

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {'refresh': str(refresh),
                'access_token': str(refresh.access_token)}
