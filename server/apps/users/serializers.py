from rest_framework import serializers
from . import services, models


class UserRegistrationSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    email = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=50)
    password_confirm = serializers.CharField(write_only=True, max_length=50)

    def validate_username(self, username):
        return services.UserSerializerService.validate_username(username)

    def validate_email(self, email):
        return services.UserSerializerService.validate_email(email)

    def validate(self, data):
        services.UserSerializerService.validate(data)


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=50)


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'role')
