from django.urls import path

from server.apps.vote_service.views import VoteAPIView

urlpatterns = [
    path('', VoteAPIView.as_view())
]
