from django.apps import AppConfig


class VoteServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server.apps.vote_service'
    verbose_name = 'Голосования'
