from django.contrib import admin
from django.db.models import Count

from . import models
from .models import Vote, Voting


@admin.register(models.Voting)
class VotingAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'start_voting')
    list_filter = ('start_voting',)
    search_fields = ('id', 'title')
    list_per_page = 10


@admin.register(models.Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'product', 'category', 'promotion',
                    'date_time_voted', 'voting')
    list_filter = ('product', 'category', 'promotion', 'date_time_voted', 'voting')
    search_fields = ('user__username',)
    list_per_page = 10

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('user', 'product', 'category', 'promotion')

    change_list_template = 'admin/custom_change_list.html'

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def statistics(self):
        last_voting_id = Voting.objects.first()
        queryset = (Vote.objects.filter(voting_id=last_voting_id)
                    .prefetch_related('product', 'user', 'categories', 'promotion', 'voting'))
        total_votes = queryset.count()
        total_product_votes = queryset.filter(product__isnull=False).count()
        total_category_votes = queryset.filter(category__isnull=False).count()
        total_promotion_votes = queryset.filter(promotion__isnull=False).count()

        top_products = (
               queryset.values('product__title')
               .annotate(product_count=Count('product'))
               .exclude(product_count=0)
               .order_by('-product_count'))[:5]
        top_categories = (
               queryset.values('category__title')
               .annotate(category_count=Count('category'))
               .exclude(category_count=0)
               .order_by('-category_count'))[:5]
        top_promotions = (
               queryset.values('promotion__title')
               .annotate(promotion_count=Count('promotion'))
               .exclude(promotion_count=0)
               .order_by('-promotion_count'))[:5]

        return {
            'total_votes': total_votes,
            'total_products_votes': total_product_votes,
            'total_categories_votes': total_category_votes,
            'total_promotions_votes': total_promotion_votes,
            'top_products': top_products,
            'top_categories': top_categories,
            'top_promotions': top_promotions
        }

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['statistics'] = self.statistics()
        return super().changelist_view(request, extra_context=extra_context)
