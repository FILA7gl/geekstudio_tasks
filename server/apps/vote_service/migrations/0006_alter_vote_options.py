# Generated by Django 4.2.4 on 2023-09-11 05:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vote_service', '0005_remove_vote_is_voted'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vote',
            options={'ordering': ('-id',), 'verbose_name': 'Голос', 'verbose_name_plural': 'Голоса'},
        ),
    ]
