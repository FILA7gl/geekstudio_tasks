from rest_framework import serializers

from server.apps.common.services import Service
from server.apps.vote_service.models import Voting, Vote


class VoteService(Service):
    model = Vote


class VoteSerializerService:
    @staticmethod
    def create(request, validated_data):
        """ Система голосования в котором пользователь можеть проголосовать
        только 1 раз, за несколько полей (по желанию) в текущем голосовании"""

        user = request.context['request'].user
        queryset = (VoteService.objects()
                    .select_related('voting', 'user', 'product', 'promotion', 'category')
                    .values('voting_id', 'user_id', 'product_id', 'promotion_id', 'category_id'))

        voting_id = Voting.objects.first()
        if not voting_id:
            raise serializers.ValidationError('Активные голосования отсутствуют.')

        existing_vote = queryset.filter(user=user, voting_id=voting_id).first()
        if existing_vote:
            raise serializers.ValidationError('Вы уже проголосовали!')
        validated_data['voting'] = voting_id
        vote = VoteService.objects().create(user=user, **validated_data)
        return vote
