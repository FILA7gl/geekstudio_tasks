from rest_framework import serializers

from server.apps.vote_service.models import Vote
from server.apps.vote_service.services import VoteSerializerService


class VoteSerializer(serializers.ModelSerializer):
    user = serializers.CharField(read_only=True)
    date_time_voted = serializers.DateTimeField(read_only=True)
    voting = serializers.CharField(read_only=True)

    class Meta:
        model = Vote
        fields = '__all__'

    def create(self, validated_data):
        return VoteSerializerService.create(self, validated_data)

