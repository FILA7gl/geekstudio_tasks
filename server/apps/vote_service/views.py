from rest_framework.generics import CreateAPIView

from server.apps.vote_service.permissions import IsBuyer
from server.apps.vote_service.serializers import VoteSerializer
from server.apps.vote_service.services import VoteService


class VoteAPIView(CreateAPIView):
    queryset = (VoteService
                .objects()
                .select_related('voting', 'user', 'product', 'category', 'promotion')
                .values('voting_id', 'user_id', 'product_id', 'promotion_id', 'category_id'))
    serializer_class = VoteSerializer
    permission_classes = [IsBuyer]
