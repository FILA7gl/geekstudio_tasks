from django.db import models

from server.apps.products.models import Product, Category, Promotion
from server.apps.users.models import CustomUser


class Voting(models.Model):
    title = models.CharField(max_length=100,
                             verbose_name='Название голосования', default='')
    start_voting = models.DateTimeField(auto_now_add=True, verbose_name='Начало голосования')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name = 'голосование'
        verbose_name_plural = 'голосования'


class Vote(models.Model):
    voting = models.ForeignKey(Voting, on_delete=models.CASCADE, verbose_name='Голосование')
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE,
                             verbose_name='Пользователь')
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, blank=True,
                                null=True, verbose_name='Продукт', related_name='votes')
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, blank=True,
                                 null=True, verbose_name='Категория')
    promotion = models.ForeignKey(Promotion, on_delete=models.SET_NULL, blank=True,
                                  null=True, verbose_name='Акция')
    date_time_voted = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата и время голосования')

    def __str__(self):
        return f'{self.user}'

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Голос'
        verbose_name_plural = 'Голоса'
