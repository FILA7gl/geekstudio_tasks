import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings.base")
app = Celery("settings")
app.config_from_object("server.settings.base", namespace="CELERY")
app.autodiscover_tasks()
