from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from server.apps.vote_service.admin import VoteAdmin
from server.settings.swagger import urlpatterns as swagger
from server.settings.base import MEDIA_URL, MEDIA_ROOT


urlpatterns = [
    path('admin/', admin.site.urls),

    # products
    path('api/v1/', include('server.apps.products.urls')),

    # users
    path('api/v1/users/', include('server.apps.users.urls')),

    # oauth2
    path('api/v1/social-auth/', include('server.apps.social_auth.urls')),

    # votes
    path('api/v1/votes/', include('server.apps.vote_service.urls')),

] + static(MEDIA_URL, document_root=MEDIA_ROOT)

urlpatterns += swagger

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls))
    ] + urlpatterns
